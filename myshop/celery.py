import os
from celery import Celery

"""
1. We set the DJANGO_SETTINGS_MODULE variable for the Celery
    command-line program.
2. We create an instance of the application with app =
    Celery('myshop') .
3. We load any custom configuration from our project settings
    using the config_from_object() method. The namespace attribute
    specifies the prefix that Celery-related settings will have in
    our settings.py file. By setting the CELERY namespace, all Celery
    settings need to include the CELERY_ prefix in their name (for
    example, CELERY_BROKER_URL ).
4. Finally, we tell Celery to auto-discover asynchronous tasks
    for our applications. Celery will look for a tasks.py file in eachapplication directory of apps added to INSTALLED_APPS in order
    to load asynchronous tasks defined in it.
"""

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'myshop.settings')

app = Celery('myshop')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()